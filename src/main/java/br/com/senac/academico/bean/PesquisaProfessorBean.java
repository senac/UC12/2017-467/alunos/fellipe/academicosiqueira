package br.com.senac.academico.bean;

import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Professor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "pesquisaProfessorBean")
@ViewScoped
public class PesquisaProfessorBean extends Bean {

    private Professor professorSelecionado;
    private List<Professor> lista;
    private ProfessorDAO dao;

    private String codigo;
    private String nome;

    public PesquisaProfessorBean() {

    }

    @PostConstruct
    public void init() {
        try {
            dao = new ProfessorDAO();
            professorSelecionado = new Professor();
            lista = dao.findAll();
        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Falha ao Carregar itens.");
        }
    }

    public void pesquisa() {
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void salvar() {

        if (this.professorSelecionado.getId() == 0) {

        } else {
            dao.update(professorSelecionado);
        }

    }

    public Professor getProfessorSelecionado() {
        return professorSelecionado;
    }

    public void setProfessorSelecionado(Professor professorSelecionado) {
        this.professorSelecionado = professorSelecionado;
    }

    public List<Professor> getLista() {
        return lista;
    }

    public void setLista(List<Professor> lista) {
        this.lista = lista;
    }

    public ProfessorDAO getDao() {
        return dao;
    }

    public void setDao(ProfessorDAO dao) {
        this.dao = dao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
