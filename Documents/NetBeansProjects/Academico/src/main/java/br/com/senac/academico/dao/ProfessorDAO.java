package br.com.senac.academico.dao;

import br.com.senac.academico.model.Professor;

public class ProfessorDAO extends DAO<Professor> {

    public ProfessorDAO() {
        super(Professor.class);
    }

    public static void main(String[] args) {

        ProfessorDAO dao = new ProfessorDAO();
        Professor professor = new Professor();
        /*
        Professor professor = daoProfessor.find(6);
         */
        professor.setNome("Guardiolaaaaaa");
        professor.setCidade("Manchester");
        professor.setEmail("dsfdsfsdfds");
        professor.setCpf("123456789");
        professor.setEndereco("sfdf");
        professor.setNumero(14);

        dao.save(professor);

    }

}
