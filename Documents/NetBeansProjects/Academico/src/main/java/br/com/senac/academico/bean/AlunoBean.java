package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.model.Aluno;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.view.ViewScoped;

@Named(value = "alunoBean")
@ViewScoped
public class AlunoBean implements Serializable {

    private Aluno aluno;
    private AlunoDAO daoAluno;

    public AlunoBean() {
        this.aluno = new Aluno();
        this.daoAluno = new AlunoDAO();

    }

    public void salvar() {
        if (this.aluno.getId() == 0) {
            daoAluno.save(aluno);
        } else {
            daoAluno.update(aluno);
        }

    }

    public String novo() {
        this.aluno = new Aluno();
        return "";
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

}
