<%@page import="br.com.senac.agenda.model.Usuario"%>
<%@page import="java.util.List"%>

<jsp:include page="../header.jsp" />

<style>

    body { 

        /*
        /* The image used */
        background-image: url("../resources/imagens/messi_comemorando.jpg");

        /* Full height */
        height: 100%; 

        /* Center and scale the image nicely */
        background-position: center top ;
        background-repeat: no-repeat  ;
        background-attachment: fixed;
        background-size: cover ;
    }

</style>



<script type="text/javascript">

    function excluir(id, nome) {

        $('#nomeUsuario').text(nome);

        var formNome = $('#txtNome').val();
        var formId = $('#txtCodigo').val();

        var url = './ExcluirUsuarioServlet?id=' + id + "&formNome=" + formNome + "&formId=" + formId;

        $('#btnConfirmar').attr('href', url);

    }


</script>





<% List<Usuario> lista = (List) request.getAttribute("lista"); %>
<% String menssagem = (String) request.getAttribute("mensagem");%>

<% if (menssagem != null) {%>

<div class="alert-danger">
    <%= menssagem%>

</div>
<% } %>





<fieldset>
    <legend  style="color: #ffffff" >Pesquisa de Usurios</legend>
    <form class="form-inline" action="./PesquisaUsuarioServlet" >

        <div class="form-group" style="padding: 20px; "  >
            <label for="txtCodigo" style="margin-right: 10px; color: #ffffff" >C�digo:</label>
            <input name="id" class="form-control form-control-sm" id="txtCodigo" type="text"/>
        </div>
        <div class="form-group">
            <label for="nome"   style="margin-right: 10px; color: #ffffff" >Nome:</label>
            <input name="nome" id="txtNome" class="form-control form-control-sm" type="text"/>
        </div>
        <button style="margin-left: 10px" type="submit" class="btn btn-default" >Pesquisar</button>

        <a href="./gerenciarUsuario.jsp" style="margin-left: 10px" class="btn btn-success" role="button" aria-disabled="true">
            <i class="fa fa-plus" aria-hidden="true"></i>Adicionar
        </a>


    </form>
</fieldset>

<hr/>



<table class="table table-hover" >
    <thead>
        <tr>
            <th  style="color: #ffffff" >C�digo</th> <th  style="color: #ffffff" >Nome</th>  <th></th>
        </tr>

    </thead>




    <% if (lista != null && lista.size() > 0) {
            for (Usuario u : lista) {
    %>
    <tr>
        <td  style=" text-shadow: 1px 1px 2px #333; color: #ffffff" ><%= u.getId()%></td><td  style=" text-shadow: 1px 1px 2px #333; color: #ffffff" ><%= u.getNome()%></td>

        <td style="width: 110px;">
            <a href='./SalvarUsuarioServlet?id=<%= u.getId()%>'   style="margin-right: 10px;" >
                <img src="../resources/imagens/user-edit-icon.png" />

            </a>
            <a href="" data-toggle="modal" data-target="#modalExclusao" 
               onclick="excluir(<%= u.getId()%>, '<%= u.getNome()%>');">
                <img src="../resources/imagens/user-delete-icon.png" />
            </a>
        </td>


    </tr>




    <% } // for

    } else {%>

    <tr >
        <td  colspan="2"  style="color: #ffffff" >N�o Existem registros.</td>
    </tr>

    <% }%>



</table>



<!-- Modal -->
<div class="modal fade" id="modalExclusao" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exclus�o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja realmente apagar o usurio <span id="nomeUsuario"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                <a  id="btnConfirmar" class="btn btn-primary">Confirmar</a>
            </div>
        </div>
    </div>
</div>



<jsp:include page="../footer.jsp" />


