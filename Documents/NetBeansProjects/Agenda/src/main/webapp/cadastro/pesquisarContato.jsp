<%@page import="br.com.senac.agenda.model.Cadastro"%>
<%@page import="java.util.List"%>
<jsp:include page="../header.jsp"/>

<style>

    body { 

        /*
        /* The image used */
        background-image: url("../resources/imagens/messi_comemorando.jpg");

        /* Full height */
        height: 100%; 

        /* Center and scale the image nicely */
        background-position: center top ;
        background-repeat: no-repeat  ;
        background-attachment: fixed;
        background-size: cover ;
    }

</style>

<script type="text/javascript">
    function deletar(id, nome) {
        $('#nomeContato').text(nome);

        var formNome = $('#txtNome').val();
        var formId = $('#txtCodigo').val();

        var url = './DeletarContatoServlet?id=' + id + "&formNome=" + formNome + "&formId=" + formId;

        $('#btnConfirmar').attr('href', url);

    }


</script>


<% List<Cadastro> lista = (List) request.getAttribute("lista");%>
<% String mensagem = (String) request.getAttribute("mensagem");%>

<% if (mensagem != null) {%>

<div class="alert-danger">
    <%= mensagem%>

</div>
<% }%>


<fieldset style=" text-shadow: 1px 1px 2px #333; color: #ffffff" >
    <legend>Pesquisa de Contato</legend>
    <form  action="./PesquisarContatoServlet" >

        <div class="row"  >

            <div class="col-2 form-group" >
                <label for="txtCodigo" style="margin-right: 10px; ">C�digo:</label>
                <input name="id" class="form-control form-control-sm" id="txtCodigo" type="text"/>
            </div>
            <div class="col-6 form-group">
                <label for="nome" style="margin-right: 10px;  " >Nome:</label>
                <input name="nome" id="txtNome" class="form-control form-control-sm" type="text"/>
            </div>

            <div class="col-2 form-group ">
                <label for="uf" style="margin-right: 10px;" >Estado:</label>           
                <select name="uf" id="txtUf" class="form-control form-control-sm">
                    <option selected value="" style="margin-right: 10px;">Estado</option>
                    <option value="es">ES</option>
                    <option value="rj">RJ</option>
                    <option value="sp">SP</option>
                </select>
            </div>

        </div>

        <button style="margin-right: 10px;" type="submit" class="btn btn-default" > <i class="fa fa-search "></i> Pesquisar</button>
        <a href="./cadastrarContato.jsp" style="margin-left: 10px" class="btn btn-success" role="button" aria-disabled="true">
            <i class="fa fa-plus" aria-hidden="true"></i>Adicionar
        </a>

    </form>
</fieldset>

<hr/>

<table class=" table table-hover">
    <thead>
        <tr style=" text-shadow: 1px 1px 2px #333; color: #ffffff">
            <th>C�digo</th> <th>Nome</th> <th>UF</th>  <th>Endere�o</th> <th>Telefone</th> <th>Celular</th> <th>Fax</th> <th>E-mail</th>
        </tr>    

    </thead>

    <% if (lista != null && lista.size() > 0) {
            for (Cadastro u : lista) {
    %>

    <tr style=" text-shadow: 1px 1px 2px #333; color: #ffffff">
        <td> <%= u.getId()%> </td> <td> <%= u.getNome()%> </td> <td><%= u.getUf()%></td> <td><%= u.getEndereco()%></td> <td><%= u.getTelefone()%></td> <td><%= u.getCelular()%></td> <td><%= u.getFax()%></td> <td><%= u.getEmail()%></td>

        <td style="width: 110px;">
            <a href='./CadastrarContatoServlet?id=<%= u.getId()%>' style="margin-right: 10px;" >
                <img src="../resources/imagens/user-edit-icon.png" />

            </a>
            <a href="" data-toggle="modal" data-target="#modalExclusao" 
               onclick="deletar(<%= u.getId()%>, '<%= u.getNome()%>');">
                <img src="../resources/imagens/user-delete-icon.png" />
            </a>
        </td>



    </tr>

    <% } // for

    } else {%>

    <tr>
        <td colspan="2">N�o existem registros.</td>

    </tr>

    <% }%>


</table>


<!-- Modal -->
<div class="modal fade" id="modalExclusao" tabindex="-1" role="dialog" 
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Exclus�o</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseja realmente apagar o contato <span id="nomeContato"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                <a  id="btnConfirmar" class="btn btn-primary">Confirmar</a>
            </div>
        </div>
    </div>
</div>





<jsp:include page="../footer.jsp"/>
