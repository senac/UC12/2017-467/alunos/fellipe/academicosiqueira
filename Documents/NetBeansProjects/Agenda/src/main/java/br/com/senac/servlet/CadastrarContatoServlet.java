/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.CadastroDAO;
import br.com.senac.agenda.model.Cadastro;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "CadastrarContatoServlet", urlPatterns = {"/cadastro/CadastrarContatoServlet"})
public class CadastrarContatoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cadastro cadastro = null;
        String erro = null;
        String id = request.getParameter("id");

        try {

            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            CadastroDAO dao = new CadastroDAO();

            cadastro = dao.get(codigo);
            request.setAttribute("cadastro", cadastro);

        } catch (Exception ex) {
            erro = "Contato não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrarContato.jsp");

        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String nome = request.getParameter("nome");
        String telefone = request.getParameter("telefone");
        String celular = request.getParameter("celular");
        String fax = request.getParameter("fax");
        String cep = request.getParameter("cep");
        String endereco = request.getParameter("endereco");
        String numero = request.getParameter("numero");
        String bairro = request.getParameter("bairro");
        String cidade = request.getParameter("cidade");
        String uf = request.getParameter("uf");
        String email = request.getParameter("email");

        String mensagem = null;
        String erro = null;

        try {

            Cadastro cadastro = new Cadastro();

            int codigo;
            try {
                codigo = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                codigo = 0;
            }

            cadastro.setId(codigo);
            cadastro.setNome(nome);
            cadastro.setTelefone(telefone);
            cadastro.setCelular(celular);
            cadastro.setFax(fax);
            cadastro.setCep(cep);
            cadastro.setEndereco(endereco);
            cadastro.setNumero(numero);
            cadastro.setBairo(bairro);
            cadastro.setCidade(cidade);
            cadastro.setUf(uf);
            cadastro.setEmail(email);

            CadastroDAO dao = new CadastroDAO();
            dao.salvar(cadastro);
            mensagem = "Salvo com sucesso!";
            request.setAttribute("cadastro", cadastro);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception ex) {
            erro = "Erro ao cadastrar usuario!";
            request.setAttribute("erro", erro);

        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrarContato.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
