package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Cadastro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CadastroDAO extends DAO<Cadastro> {

    @Override
    public void salvar(Cadastro cadastro) {

        Connection connection = null;
        try {

            String query;
            if (cadastro.getId() == 0) {
                query = "insert into cadastro (nome, telefone, celular, fax, cep, endereco, numero, bairro, cidade, uf, email ) values (?, ?, ?, ?, ?,?, ?,?, ?, ?, ? );";

            } else {
                query = "update cadastro set nome = ? , telefone = ?, celular = ?, fax = ?, cep = ?, endereco = ?, numero = ?, bairro = ?, cidade = ?, uf = ?, email = ? where id = ? ;";
            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, cadastro.getNome());
            ps.setString(2, cadastro.getTelefone());
            ps.setString(3, cadastro.getCelular());
            ps.setString(4, cadastro.getFax());
            ps.setString(5, cadastro.getCep());
            ps.setString(6, cadastro.getEndereco());
            ps.setString(7, cadastro.getNumero());
            ps.setString(8, cadastro.getBairo());
            ps.setString(9, cadastro.getCidade());
            ps.setString(10, cadastro.getUf());
            ps.setString(11, cadastro.getEmail());
            if (cadastro.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                cadastro.setId(rs.getInt(1));

            } else {
                ps.setInt(12, cadastro.getId());
                ps.executeUpdate();

            }
        } catch (Exception ex) {

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }

        }

    }

    @Override
    public void deletar(Cadastro cadastro) {

        String query = "delete from cadastro where id = ? ";
        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, cadastro.getId());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Erro ao deletar registro....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }

        }
    }

    @Override
    public List<Cadastro> listar() {
        String query = "select * from cadastro;";
        List<Cadastro> lista = new ArrayList<>();
        Connection connection = null;
        try {
            connection = Conexao.getConnection(); //abriu conexção com o banco
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query); //executar a query e retornar uma "tabela"
            while (rs.next()) {
                Cadastro cadastro = new Cadastro();
                cadastro.setId(rs.getInt("id"));
                cadastro.setNome(rs.getString("nome"));
                cadastro.setTelefone(rs.getString("telefone"));
                cadastro.setCelular(rs.getString("celular"));
                cadastro.setFax(rs.getString("fax"));
                cadastro.setCep(rs.getString("cep"));
                cadastro.setEndereco(rs.getString("endereco"));
                cadastro.setNumero(rs.getString("numero"));
                cadastro.setBairo(rs.getString("bairro"));
                cadastro.setCidade(rs.getString("cidade"));
                cadastro.setUf(rs.getString("uf"));
                cadastro.setEmail(rs.getString("email"));
                lista.add(cadastro);

            }

        } catch (Exception ex) {
            System.out.println("Ocorreu um erro ao fazer a consulta....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }

        }

        return lista;
    }

    @Override
    public Cadastro get(int id) {
        Cadastro cadastro = null;
        Connection connection = null;
        String query = "select * from cadastro where id = ? ; ";
        try {
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                cadastro = new Cadastro();
                cadastro.setId(rs.getInt("id"));
                cadastro.setNome(rs.getString("nome"));
                cadastro.setTelefone(rs.getString("telefone"));
                cadastro.setCelular(rs.getString("celular"));
                cadastro.setFax(rs.getString("fax"));
                cadastro.setCep(rs.getString("cep"));
                cadastro.setEndereco(rs.getString("endereco"));
                cadastro.setNumero(rs.getString("numero"));
                cadastro.setBairo(rs.getString("bairro"));
                cadastro.setCidade(rs.getString("cidade"));
                cadastro.setUf(rs.getString("uf"));
                cadastro.setEmail(rs.getString("email"));

            }

        } catch (Exception ex) {
            System.out.println("Erro ao executar a consulta....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexão....");
            }

        }

        return cadastro;
    }

    public List<Cadastro> getByFiltro(Integer id, String nome, String uf) {

        List<Cadastro> lista = new ArrayList<>();
        Connection connection = null;

        try {
            StringBuilder sb = new StringBuilder("select * from cadastro where 1 = 1");

            if (id != null) {
                sb.append(" and id  = ? ");

            }

            if (nome != null && !nome.trim().isEmpty()) {
                sb.append(" and nome like ? ");

            }
            if (uf != null && !uf.trim().isEmpty()) {
                sb.append(" and uf like ? ");

            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(sb.toString());
            int index = 0;
            if (id != null) {
                ps.setInt(++index, id);

            }
            if (nome != null && !nome.trim().isEmpty()) {
                ps.setString(++index, "%" + nome + "%");

            }
            if (uf != null && !uf.trim().isEmpty()) {
                ps.setString(++index, "%" + uf + "%");

            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Cadastro cadastro = new Cadastro();
                cadastro.setId(rs.getInt("id"));
                cadastro.setNome(rs.getString("nome"));
                cadastro.setUf(rs.getString("uf"));
                cadastro.setTelefone(rs.getString("telefone"));
                cadastro.setCelular(rs.getString("celular"));
                cadastro.setFax(rs.getString("fax"));
                cadastro.setCep(rs.getString("cep"));
                cadastro.setEndereco(rs.getString("endereco"));
                cadastro.setNumero(rs.getString("numero"));
                cadastro.setBairo(rs.getString("bairro"));
                cadastro.setCidade(rs.getString("cidade"));
                cadastro.setEmail(rs.getString("email"));
                lista.add(cadastro);

            }
        } catch (Exception ex) {
            System.out.println("Erro ao realizar consulta");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao....");
            }
        }
        return lista;
    }

}
